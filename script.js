const form = document.querySelector(".form");
let first = document.getElementById("fname");
let last = document.getElementById("lname");
let firstError = document.querySelector(".first-error");
let secondError = document.querySelector(".second-error");
let successMsg = document.querySelector(".success");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  let firstName = first.value;
  let lastName = last.value;
  if (firstName === "" && lastName === "") {
    secondError.textContent = "Missing Last name";
    firstError.textContent = "Missing First name";
    successMsg.textContent = "";
  } else if (firstName === "") {
    firstError.textContent = "Missing First name";
    secondError.textContent = "";
    successMsg.textContent = "";
  } else if (lastName === "") {
    firstError.textContent = "";
    secondError.textContent = "Missing Last name";
    successMsg.textContent = "";
  } else {
    successMsg.textContent = "Success";
    firstError.textContent = "";
    secondError.textContent = "";
    first.value = "";
    last.value = "";
  }
});
